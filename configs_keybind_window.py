# Keybind window
# Shows list of key bindings for selected WM, only qtile for now, add i3wm and possibly xmonad later
# TODO: Add ability to reconfigure keybinds, add better descriptions to xmonad actions

import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import re
import math

# Defines window displayed to the user after selecting a window manager with the list of keybinds and actions from their current configuration file
# Assumes their config file is in the default location in the user's home directory
class configs_keybind_window(Gtk.Window):

    def __init__(self, keybind_text_display, wm_name):

        wm_name = wm_name + ' keybinds'
        Gtk.Window.__init__(self, title=wm_name)
        self.resize(1200,200)
        Gtk.Window.set_position(self, Gtk.WindowPosition.MOUSE)
        self.keybind_text_display_local = Gtk.Entry()
        self.keybind_text_display_local = keybind_text_display
        self.wm_to_config = ''
        self.config_file_name = ''
        self.current_page = 1             # current page for bindings, use as a multiplier
        self.last_bind = 0                # last bind to display
        self.max_pages = 0                # total number of keybind pages

        # Class Variables
        self.key_binds = []              # all bound key combinations
        self.key_actions = []            # all actions associated with key binds
        self.keybind_grids = []          # list of grids with each of the keybinds
        self.keybind_comments = []       # list of all comments related to binds
        self.num_binds = 0               # num of binds in list
        self.source_bind_list = []
        self.source_bind_action = ''
        self.keybind_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.keybind_header = Gtk.HeaderBar()
        self.keyback_button = Gtk.Button()
        self.keyforward_button = Gtk.Button()
        self.key_search_button = Gtk.Button('Search')
        self.len_key_binds = 0
        self.key_action_label = ''
        self.key_bind_label = ''
        self.key_page_label = ''
        self.user_query = ''

        self.keybind_grid = Gtk.Grid() 

        # Initialize the headerbar
        self.keybind_header.set_show_close_button(True)
        self.keybind_header.props.title = 'Keybinds'
        self.set_titlebar(self.keybind_header)

        # Start a box for the back and forward buttons to link them
        Gtk.StyleContext.add_class(self.keybind_box.get_style_context(), 'linked')

        # Populate headerbar
        self.key_search_button.connect('clicked', search_binds_dialog, self, self.key_actions, self.key_binds)
        self.keybind_box.add(self.key_search_button)
        self.keyback_button.add(Gtk.Arrow(Gtk.ArrowType.LEFT, Gtk.ShadowType.NONE))
        self.keyback_button.connect('clicked', self.flip_page, -1)
        self.keybind_box.add(self.keyback_button)
        self.keyforward_button.add(Gtk.Arrow(Gtk.ArrowType.RIGHT, Gtk.ShadowType.NONE))
        self.keyforward_button.connect('clicked', self.flip_page, 1)
        self.keybind_box.add(self.keyforward_button)
        self.key_page_label = Gtk.Label.new(f'{self.current_page} / {self.max_pages}')
        self.keybind_box.add(self.key_page_label) 


        # Add the headerbar
        self.keybind_header.pack_start(self.keybind_box)

    # Set variables related to WM selected by the user
    def set_wm(self, wm_input):

        self.wm_to_config = wm_input

        if wm_input == 'i3 / i3-gaps':
            config_file_name = os.environ['HOME'] + '/.config/i3/config'
        elif wm_input == 'qtile':
            config_file_name = os.environ['HOME'] + '/.config/qtile/config.py'
        elif wm_input == 'xmonad':
            config_file_name = os.environ['HOME'] + '/.xmonad/xmonad.hs'
        self.num_binds = 1

        # Read in the config file, process line by line to determine the key binds
        # Only processes qtile for now, will process other configuration files in the future
        if os.path.isfile(config_file_name):
            with open('%s' % config_file_name, 'r' ) as f:
                if wm_input == 'qtile':
                    for x in f:
                        if not x: continue
                        if 'keys.append' in x:
                            keybind_text = f.readline()
                            keybind_full_text = ''
                            while keybind_text.strip(','):
                                # Remove the @ delimiter here and figure out how to tokenize this to separate the binding from the action
                                keybind_full_text = keybind_full_text + '@' + keybind_text.strip(',')
                                if 'lazy' in keybind_text.strip(): break
                            self.key_binds.append(keybind_full_text)
                            self.key_actions.append(keybind_full_text)
                        elif 'Key(' in x:
                            keybind_text = f.readline()
                            keybind_full_text = ''
                            self.key_binds.append(self.replace_binds(source_bind=keybind_text.strip(), wm_input=wm_input, bind_or_action='bind'))
                            keybind_text = f.readline()
                            while keybind_text.strip():
                                if keybind_text.strip() == '),': break
                                elif keybind_text.strip() != 'Key(':
                                    keybind_full_text = keybind_full_text + ' ' + self.replace_binds(source_bind=keybind_text.strip(), wm_input=wm_input, bind_or_action='action')
                                keybind_text = f.readline()
                            self.key_actions.append(keybind_full_text)
                            self.num_binds += 1
                elif wm_input == 'i3 / i3-gaps':
                    for x in f:
                        if not x: continue
                        elif 'bindsym' in x:
                            keybind_text = x.strip('\n')
                            keybind_full_list = keybind_text.split(' ')
                            if len(keybind_full_list) > 1:
                                keybind_text = keybind_full_list[1]
                                keybind_full_text = ''
                                self.key_binds.append(self.replace_binds(source_bind=keybind_text, wm_input=wm_input, bind_or_action='bind'))
                                keybind_full_list = keybind_full_list[2:]
                                for action in keybind_full_list:
                                    keybind_full_text = keybind_full_text + ' ' + action
                                self.key_actions.append(keybind_full_text)
                                self.num_binds += 1
                elif wm_input == 'xmonad':
                    for x in f:
                        if not x: continue
                        elif 'xK_' in x:
                            keybind_text = x.strip('\n')
                            keybind_full_list = x.strip('\n').split(' ')
                            if len(keybind_full_list) > 1:
                                for index, value in enumerate(keybind_full_list):
                                    keybind_text = keybind_text + value
                                    if ')' in keybind_text:
                                        break
                                keybind_full_text = ''
                                self.key_binds.append(self.replace_binds(source_bind=keybind_text.split(')')[0], wm_input=wm_input, bind_or_action='bind'))
                                keyaction_full_list = keybind_full_list[1:]
                                for action in keyaction_full_list:
                                    keybind_full_text = keybind_full_text + ' ' + action
                                self.key_actions.append(self.replace_binds(source_bind=keybind_full_text,wm_input=wm_input, bind_or_action='action'))
                                self.num_binds += 1
                else:
                    not_supported_dialog(self,wm_input)
                    self.destroy()
                    return
    
        else:
            config_not_found_dialog(self)
            self.destroy()
            return

        self.max_pages = self.num_binds / 10
        if self.num_binds % 10 != 0:
            self.max_pages += 1
            self.max_pages = math.trunc(self.max_pages)

        self.keybind_box.remove(self.key_page_label)
        self.key_page_label = Gtk.Label.new(f'{self.current_page} / {self.max_pages}')
        self.keybind_box.add(self.key_page_label)
        self.keybind_box.show_all()
        self.keybind_header.show_all()
        self.populate_grids()

        self.set_grid(current_grid=self.keybind_grids[0], old_grid=None)
    
    # Set currently displayed grid
    def set_grid(self, current_grid, old_grid):
        
        # remove old grid
        if old_grid:
            self.remove(old_grid)
        
        # add the grid to the window and fill with the keybinds.
        self.add(current_grid)
        self.len_key_binds = len(self.key_binds)
        
        # Ask to redraw the grid
        self.show_all()
        self.keybind_grids[self.current_page-1].queue_draw()


    # Populate each page (grid) with binds
    def populate_grids(self):

        k = 0
        j = 0
        while k < self.max_pages:
            self.keybind_grids.append(Gtk.Grid())
            header_action = Gtk.Label.new('Action')
            header_bind = Gtk.Label.new('Bind')
            self.keybind_grids[k].attach(child=header_action, left=0, top=0, width=1, height=1)
            self.keybind_grids[k].attach_next_to(child=header_bind, sibling=header_action, side=Gtk.PositionType.RIGHT, width=2, height=1)
            if self.wm_to_config == 'qtile':
                header_comment = Gtk.Label.new('Comment')
                self.keybind_grids[k].attach_next_to(child=header_comment, sibling=header_bind, side=Gtk.PositionType.RIGHT, width=1, height=1)
            while j < len(self.key_binds) and j < ((k + 1) * 10):
                key_action_label = Gtk.Label.new('%s' % self.key_actions[j]) 
                key_bind_label = Gtk.Label.new('%s' % self.key_binds[j])
                if self.wm_to_config == 'qtile':
                    key_comment_label = Gtk.Label.new('%s' % self.keybind_comments[j])
                self.keybind_grids[k].attach(child=key_action_label, left=0, top=(j%10) + 1, width=1, height=1)
                self.keybind_grids[k].attach_next_to(child=key_bind_label, sibling=key_action_label, side=Gtk.PositionType.RIGHT, width=2, height=1)
                if self.wm_to_config == 'qtile':
                    self.keybind_grids[k].attach_next_to(child=key_comment_label, sibling=key_bind_label, side=Gtk.PositionType.RIGHT, width=1, height=1)
                j+=1    
            k += 1

    # Clean up key combo and action text, this seems bad, need to rewrite it
    def replace_binds(self, source_bind, wm_input, bind_or_action):

        if wm_input == 'qtile':
            # List of search pattern and replacment
            clean_strings = { '[': '', ']': '', '"': '', ',': '', '   ': '', '.': ''}
            mods = {'mod': 'mod +','shift': 'shift +', 'control': 'control +'}
            clean_strings.update(mods)
            lazyFx = {'.spawn': 'Launch ', '.layout': 'Set Current Layout: ', 'lazy': '',} 
            clean_strings.update(lazyFx)

            # Perform replacements using regex
            clean_strings = dict((re.escape(k), v) for k, v in iter(clean_strings.items()))
            pattern = re.compile('|'.join(clean_strings.keys()))
            source_bind = pattern.sub(lambda m: clean_strings[re.escape(m.group(0))], source_bind)

            if bind_or_action == 'bind':
                split_bind = source_bind.split('#')
                source_bind = split_bind[0]
            elif bind_or_action == 'action':
                split_bind = source_bind.split('#')
                source_bind = split_bind[0]
                if len(split_bind) > 1:
                    self.keybind_comments.append(split_bind[1].strip())
                else:
                    self.keybind_comments.append('No Comment')

            return source_bind
        elif wm_input == 'i3 / i3-gaps':

            # List of search pattern and replacment
            clean_strings = { 'bindsym': '', '[': '', ']': '', '"': '', ',': '', '   ': '', '.': ''}
            mods = {'$mod': 'Alt','shift': 'Shift ', 'control': 'Control', '$sup': 'Super'}
            clean_strings.update(mods)
            lazyFx = {'exec --no-startup-id': 'Launch ', '.layout': 'Set Current Layout: ', 'lazy': '',} 
            clean_strings.update(lazyFx)

            # Perform replacements using regex
            clean_strings = dict((re.escape(k), v) for k, v in iter(clean_strings.items()))
            pattern = re.compile('|'.join(clean_strings.keys()))
            source_bind = pattern.sub(lambda m: clean_strings[re.escape(m.group(0))], source_bind)

            return source_bind
        elif wm_input == 'xmonad':

            # Figure out how to pull in the action without the bind
            if bind_or_action == 'action':
                self.source_bind_list = source_bind.split(')')
                self.source_bind_action = ''
                for i, source_bind_token in enumerate(self.source_bind_list):
                    if i != 0:
                        self.source_bind_action = self.source_bind_action + source_bind_token
            else:
                self.source_bind_action = source_bind

            # List of search pattern and replacment
            clean_strings = { '. | .': '+', 'bindsym': '', '[': '', ']': '', '"': '', ',': '', '   ': '', '.': '', '(': '', ')': ''}
            mods = {'mod4Mask': 'Super +','shift': 'Shift +', 'control': 'Control +', 'modMask': 'Alt + ', 'Mask': 'Alt +', 'xK_': ''}
            clean_strings.update(mods)
            lazyFx = {'exec --no-startup-id': 'Launch ', '.layout': 'Set Current Layout: ', 'lazy': '', '|': ''} 
            clean_strings.update(lazyFx)
            
            # Perform replacements using regex
            clean_strings = dict((re.escape(k), v) for k, v in iter(clean_strings.items()))
            pattern = re.compile('|'.join(clean_strings.keys()))
            source_bind = pattern.sub(lambda m: clean_strings[re.escape(m.group(0))], self.source_bind_action)
            
            return source_bind

    # User wants to either flip to the next or previous page
    # direction: -1 for previous page, 1 for next page
    # button is passed by the callback
    def flip_page(self, button, direction):

        page_check = 0
        replaced_grid = self.keybind_grids[self.current_page-1]
        
        if direction == -1 and self.current_page != 1:
            page_check = 1
        elif direction == 1 and self.current_page != self.max_pages:
            page_check = 1
        if page_check == 1:
            self.current_page += direction

        # Change the page number displayed in the header
        self.keybind_box.remove(self.key_page_label)
        self.key_page_label = Gtk.Label.new(f'{self.current_page} / {self.max_pages}')
        self.keybind_box.add(self.key_page_label)
        self.keybind_box.show_all()
        self.keybind_header.show_all()

        # Change the list of binds
        self.set_grid(current_grid=self.keybind_grids[self.current_page-1], old_grid=replaced_grid)

    

# Page Number debug, to be removed later once this is working.
class config_not_found_dialog(Gtk.Dialog):

    def __init__(self,parent):

        Gtk.Dialog.__init__(self, 'Error', parent, 0,
            (Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(150, 100)
        label = Gtk.Label('Configuration file not found!')
        box = self.get_content_area()
        box.add(label)
        self.show_all()
        response = self.run()
        if response == Gtk.ResponseType.OK:
            self.destroy()
            return

# Tell the user that while there is a button and a config file
# this WM is not currently supported by Configs
class not_supported_dialog(Gtk.Dialog):

    def __init__(self,parent,wm_name):

        Gtk.Dialog.__init__(self, 'Error', parent, 0,
            (Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(150, 100)
        label = Gtk.Label('%s is not supported yet.' % wm_name)
        box = self.get_content_area()
        box.add(label)
        self.show_all()
        response = self.run()
        if response == Gtk.ResponseType.OK:
            self.destroy()
            return

# Show the user the result of their search
# Label: Top line of new dialog
# Label2: Bottom line of new dialog
class search_results_dialog(Gtk.Dialog):

    def __init__(self, parent, kb_parent, found_record_action, found_record_bind):

        Gtk.Dialog.__init__(self, 'Search Results', parent, 0,
            (Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(300,200)
        box = self.get_content_area()
        self.kb_parent = kb_parent
        self.search_action_result = Gtk.Entry()
        self.search_bind_result = Gtk.Entry()
        if len(found_record_action.get_text()):
            self.bind_id = self.search_binds(type='action', search_text=found_record_action.get_text())
            self.populate_results()
        elif len(found_record_bind.get_text()):
            self.bind_id = self.search_binds(type='bind', search_text=found_record_bind.get_text())
            self.populate_results()          
        if self.bind_id != -1:
            label = Gtk.Label('Action: %s' % self.search_action_result.get_text())
            label2 = Gtk.Label('Bind: %s' % self.search_bind_result.get_text())
        else:
            label = Gtk.Label('No such keybind found')
            label2 = Gtk.Label('Try to make the search terms more generic')  
        box.add(label)
        box.add(label2)
        self.show_all()
        response = self.run()
        if response == Gtk.ResponseType.OK:
            self.destroy()
            return

    # Set results label
    # Make sure the results labels are not editable
    def populate_results(self):

        self.search_action_result.set_editable(False)
        self.search_action_result.set_text(self.kb_parent.key_actions[self.bind_id])
        self.search_bind_result.set_editable(False)
        self.search_bind_result.set_text(self.kb_parent.key_binds[self.bind_id])

    # search for text through the binds 
    # returns -1 if no match is found
    # replace comparison with regex in the future to be more useful
    def search_binds(self, type, search_text):

        if type == 'action':
            search_list = self.kb_parent.key_actions
        else:
            search_list = self.kb_parent.key_binds

        for index, value in enumerate(search_list):
            if value.find(search_text) != -1:
                break
        else:
            return -1
        return index

# Asks the user to search for either an action or a bind
# label: Search actions
# label2: Search key binds
class search_binds_dialog(Gtk.Dialog):

    def __init__(self, parent, parent_window, key_actions, key_binds):
        Gtk.Dialog.__init__(self, 'Search Results', parent_window, 0,
            (Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_default_size(300,75)
        box = self.get_content_area()
        search_action_field = Gtk.Entry()
        search_bind_field = Gtk.Entry()
        label = Gtk.Label('Search key actions:')
        label2 = Gtk.Label('Search keys:')
        box.add(label)
        box.add(search_action_field)
        box.add(label2)
        box.add(search_bind_field)
        self.show_all()
        response = self.run()
        if response == Gtk.ResponseType.OK:
            search_results_dialog(self, parent_window, search_action_field, search_bind_field)
            self.destroy()
            return
