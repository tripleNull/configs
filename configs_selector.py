# Defines wm selector window
# TODO: Display keyboard shortcuts by reading  in config files when a WM is selected
#       Add a function to save the config file 

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from configs_keybind_window import configs_keybind_window

user_wm = Gtk.Label.new("Select your WM:")
keybind_display = Gtk.Entry()
wm_grid = Gtk.Grid()

class wm_selector_window(Gtk.Window):

    def __init__(self):
        
        Gtk.Window.__init__(self, title="Window Manager Selector")
        Gtk.Window.set_position(self, Gtk.WindowPosition.MOUSE)

    def set_grid(self):

        self.add(wm_grid)

        i3_button = Gtk.Button.new_with_label("i3 / i3-gaps")
        qtile_button = Gtk.Button.new_with_label("qtile")
        xmonad_button = Gtk.Button.new_with_label("xmonad")

        wm_grid.attach(user_wm, 0, 0, Gtk.Align.CENTER, 1)
        wm_grid.attach(i3_button, 0, 1, 1, 1)
        wm_grid.attach(qtile_button, 1, 1, 1, 1)
        wm_grid.attach(xmonad_button, 2, 1, 1, 1)
        i3_button.connect("clicked", self.when_button_clicked, keybind_display, "i3wm")
        qtile_button.connect("clicked", self.when_button_clicked, keybind_display, "qtile")
        xmonad_button.connect("clicked", self.when_button_clicked, keybind_display, "xmonad")

    def when_button_clicked(self, button, kbdy, wm_name):

        kbwin = configs_keybind_window(keybind_text_display=kbdy,wm_name=wm_name)
        kbwin.set_wm(wm_input=button.get_label()) 
        kbwin.show_all()
        wm_grid.attach(keybind_display, 0, 2, Gtk.Align.CENTER, 1)

    def cleanup(self, event, widget):

      Gtk.main_quit()
