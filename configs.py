# -*- coding: utf-8 -*-
# configs.py
# Runs the main GTK loop and sets up the initial window
# Make this select the WM that the user installed, rather than from a list, unless they have multiple config files, then use tabs to switch between WMs.
# This will allow for all items to be contained on one window rather than launching a second window.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from configs_selector import wm_selector_window

wm_window = wm_selector_window()
wm_window.set_grid()        
wm_window.connect("delete-event", wm_window.cleanup)
wm_window.show_all()
Gtk.main()

